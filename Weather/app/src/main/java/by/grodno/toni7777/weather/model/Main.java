package by.grodno.toni7777.weather.model;

/**
 * Created by mymacbookpro on 6/7/16.
 */
public class Main {

    private float temp;
    private float pressure;
    private int humidity;
    private float temp_min;
    private float temp_max;

    public Main() {
    }

    public Main(float temp, float pressure, int humidity, float temp_min, float temp_max) {
        this.temp = temp;
        this.pressure = pressure;
        this.humidity = humidity;
        this.temp_min = temp_min;
        this.temp_max = temp_max;
    }

    public float getTemp() {
        return temp;
    }

    public void setTemp(float temp) {
        this.temp = temp;
    }

    public float getPressure() {
        return pressure;
    }

    public void setPressure(float pressure) {
        this.pressure = pressure;
    }

    public int getHumidity() {
        return humidity;
    }

    public void setHumidity(int humidity) {
        this.humidity = humidity;
    }

    public float getTemp_min() {
        return temp_min;
    }

    public void setTemp_min(float temp_min) {
        this.temp_min = temp_min;
    }

    public float getTemp_max() {
        return temp_max;
    }

    public void setTemp_max(float temp_max) {
        this.temp_max = temp_max;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Main main = (Main) o;

        if (Float.compare(main.temp, temp) != 0) return false;
        if (Float.compare(main.pressure, pressure) != 0) return false;
        if (humidity != main.humidity) return false;
        if (Float.compare(main.temp_min, temp_min) != 0) return false;
        return Float.compare(main.temp_max, temp_max) == 0;

    }

    @Override
    public int hashCode() {
        int result = (temp != +0.0f ? Float.floatToIntBits(temp) : 0);
        result = 31 * result + (pressure != +0.0f ? Float.floatToIntBits(pressure) : 0);
        result = 31 * result + humidity;
        result = 31 * result + (temp_min != +0.0f ? Float.floatToIntBits(temp_min) : 0);
        result = 31 * result + (temp_max != +0.0f ? Float.floatToIntBits(temp_max) : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Main{" +
                "temp=" + temp +
                ", pressure=" + pressure +
                ", humidity=" + humidity +
                ", temp_min=" + temp_min +
                ", temp_max=" + temp_max +
                '}';
    }
}

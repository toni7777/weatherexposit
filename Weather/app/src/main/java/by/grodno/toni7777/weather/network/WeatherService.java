package by.grodno.toni7777.weather.network;

import by.grodno.toni7777.weather.model.WeatherData;
import by.grodno.toni7777.weather.network.model.WeatherDataDTO;
import retrofit2.http.GET;
import retrofit2.http.Query;
import rx.Observable;

import static by.grodno.toni7777.weather.util.Constants.*;

public interface WeatherService {

//    @GET("/data/2.5/weather")
//    Observable<WeatherData> getRxWeather(@Query(QUERY_CITY) String query,
//                                         @Query(QUERY_LANGUAGE) String lang,
//                                         @Query(QUERY_APIKEY) String key);

    @GET(FORECAST_DAILY_URL)
    Observable<WeatherDataDTO> getRxWeatherDays(@Query(QUERY_CITY) String query,
                                                @Query(QUERY_LANGUAGE) String lang,
                                                @Query(QUERY_DAY_COUNT) String count,
                                                @Query(QUERY_TEMP_UNITS) String format,
                                                @Query(QUERY_APIKEY) String key);
}

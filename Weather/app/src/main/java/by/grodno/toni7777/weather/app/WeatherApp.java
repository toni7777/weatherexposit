package by.grodno.toni7777.weather.app;

import android.app.Application;

import by.grodno.toni7777.weather.network.NetworkService;


public class WeatherApp extends Application {

    private NetworkService networkService;

    @Override
    public void onCreate() {
        super.onCreate();
        networkService = new NetworkService();
    }

    public NetworkService getNetworkService() {
        return networkService;
    }
}

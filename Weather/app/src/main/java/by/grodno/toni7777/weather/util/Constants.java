package by.grodno.toni7777.weather.util;

public final class Constants {

    public static final String BASE_URL = "http://api.openweathermap.org";
    public static final String FORECAST_DAILY_URL = "/data/2.5/forecast/daily";

    public static final String QUERY_CITY = "q";
    public static final String QUERY_LANGUAGE = "lang";
    public static final String QUERY_DAY_COUNT = "cnt";
    public static final String QUERY_TEMP_UNITS = "units";
    public static final String QUERY_APIKEY = "APPID";

    public static final String APIKEY_VALUE = "7246f8da7f6f0d62458c3eb424a4ba8d";

    public static final String COUNT_WEATHER_DAYS = "14";
    public static final String TEMPERATURE_UNITS = "metric";

    private Constants() {
    }

}

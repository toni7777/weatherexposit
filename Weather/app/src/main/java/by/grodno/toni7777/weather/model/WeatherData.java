package by.grodno.toni7777.weather.model;

import java.util.List;

/**
 * Created by mymacbookpro on 6/6/16.
 */
public class WeatherData {

    private Coord coord;
    private List<Weather> weather;
    private int id;
    private String name;
    private int cod;
    private Sys sys;
    private int dt;
    private Clouds clouds;
    private Wind wind;
    private int visibility;
    private String base;
    private Main main;


    public WeatherData() {
    }

    public WeatherData(Coord coord, List<Weather> weather, int id, String name, int cod, Sys sys, int dt, Clouds clouds, Wind wind, int visibility, String base, Main main) {
        this.coord = coord;
        this.weather = weather;
        this.id = id;
        this.name = name;
        this.cod = cod;
        this.sys = sys;
        this.dt = dt;
        this.clouds = clouds;
        this.wind = wind;
        this.visibility = visibility;
        this.base = base;
        this.main = main;
    }

    public Coord getCoord() {
        return coord;
    }

    public void setCoord(Coord coord) {
        this.coord = coord;
    }

    public List<Weather> getWeather() {
        return weather;
    }

    public void setWeather(List<Weather> weather) {
        this.weather = weather;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getCod() {
        return cod;
    }

    public void setCod(int cod) {
        this.cod = cod;
    }

    public Sys getSys() {
        return sys;
    }

    public void setSys(Sys sys) {
        this.sys = sys;
    }

    public int getDt() {
        return dt;
    }

    public void setDt(int dt) {
        this.dt = dt;
    }

    public Clouds getClouds() {
        return clouds;
    }

    public void setClouds(Clouds clouds) {
        this.clouds = clouds;
    }

    public Wind getWind() {
        return wind;
    }

    public void setWind(Wind wind) {
        this.wind = wind;
    }

    public int getVisibility() {
        return visibility;
    }

    public void setVisibility(int visibility) {
        this.visibility = visibility;
    }

    public String getBase() {
        return base;
    }

    public void setBase(String base) {
        this.base = base;
    }

    public Main getMain() {
        return main;
    }

    public void setMain(Main main) {
        this.main = main;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        WeatherData that = (WeatherData) o;

        if (id != that.id) return false;
        if (cod != that.cod) return false;
        if (dt != that.dt) return false;
        if (visibility != that.visibility) return false;
        if (coord != null ? !coord.equals(that.coord) : that.coord != null) return false;
        if (weather != null ? !weather.equals(that.weather) : that.weather != null) return false;
        if (name != null ? !name.equals(that.name) : that.name != null) return false;
        if (sys != null ? !sys.equals(that.sys) : that.sys != null) return false;
        if (clouds != null ? !clouds.equals(that.clouds) : that.clouds != null) return false;
        if (wind != null ? !wind.equals(that.wind) : that.wind != null) return false;
        if (base != null ? !base.equals(that.base) : that.base != null) return false;
        return main != null ? main.equals(that.main) : that.main == null;

    }

    @Override
    public int hashCode() {
        int result = coord != null ? coord.hashCode() : 0;
        result = 31 * result + (weather != null ? weather.hashCode() : 0);
        result = 31 * result + id;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + cod;
        result = 31 * result + (sys != null ? sys.hashCode() : 0);
        result = 31 * result + dt;
        result = 31 * result + (clouds != null ? clouds.hashCode() : 0);
        result = 31 * result + (wind != null ? wind.hashCode() : 0);
        result = 31 * result + visibility;
        result = 31 * result + (base != null ? base.hashCode() : 0);
        result = 31 * result + (main != null ? main.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "WeatherData{" +
                "coord=" + coord +
                ", weather=" + weather +
                ", id=" + id +
                ", name='" + name + '\'' +
                ", cod=" + cod +
                ", sys=" + sys +
                ", dt=" + dt +
                ", clouds=" + clouds +
                ", wind=" + wind +
                ", visibility=" + visibility +
                ", base='" + base + '\'' +
                ", main=" + main +
                '}';
    }
}

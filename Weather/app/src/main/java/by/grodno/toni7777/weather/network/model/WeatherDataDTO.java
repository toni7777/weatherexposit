package by.grodno.toni7777.weather.network.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by mymacbookpro on 6/10/16.
 */
public class WeatherDataDTO {

    private CityDTO city;
    @SerializedName("cod")
    private String code;
    private String message;
    @SerializedName("list")
    private List<WeatherDayDTO> weatherDays;

    public WeatherDataDTO() {
    }

    public WeatherDataDTO(CityDTO city, String cod, String message, List<WeatherDayDTO> list) {
        this.city = city;
        this.code = cod;
        this.message = message;
        this.weatherDays = list;
    }

    public CityDTO getCity() {
        return city;
    }

    public void setCity(CityDTO city) {
        this.city = city;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<WeatherDayDTO> getWeatherDays() {
        return weatherDays;
    }

    public void setWeatherDays(List<WeatherDayDTO> weatherDays) {
        this.weatherDays = weatherDays;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        WeatherDataDTO weather16 = (WeatherDataDTO) o;

        if (city != null ? !city.equals(weather16.city) : weather16.city != null) return false;
        if (code != null ? !code.equals(weather16.code) : weather16.code != null) return false;
        if (message != null ? !message.equals(weather16.message) : weather16.message != null) return false;
        return !(weatherDays != null ? !weatherDays.equals(weather16.weatherDays) : weather16.weatherDays != null);

    }

    @Override
    public int hashCode() {
        int result = city != null ? city.hashCode() : 0;
        result = 31 * result + (code != null ? code.hashCode() : 0);
        result = 31 * result + (message != null ? message.hashCode() : 0);
        result = 31 * result + (weatherDays != null ? weatherDays.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "WeatherDataDTO{" +
                "city=" + city +
                ", code='" + code + '\'' +
                ", message='" + message + '\'' +
                ", weatherDays=" + weatherDays +
                '}';
    }
}

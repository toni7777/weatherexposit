package by.grodno.toni7777.weather.model;

/**
 * Created by mymacbookpro on 6/7/16.
 */
public class Wind {

    private float speed;
    private float deg;

    public Wind() {
    }

    public Wind(float speed, float deg) {
        this.speed = speed;
        this.deg = deg;
    }

    public float getDeg() {
        return deg;
    }

    public void setDeg(float deg) {
        this.deg = deg;
    }

    public float getSpeed() {
        return speed;
    }

    public void setSpeed(float speed) {
        this.speed = speed;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Wind wind = (Wind) o;

        if (Float.compare(wind.speed, speed) != 0) return false;
        return Float.compare(wind.deg, deg) == 0;

    }

    @Override
    public int hashCode() {
        int result = (speed != +0.0f ? Float.floatToIntBits(speed) : 0);
        result = 31 * result + (deg != +0.0f ? Float.floatToIntBits(deg) : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Wind{" +
                "speed=" + speed +
                ", deg=" + deg +
                '}';
    }
}

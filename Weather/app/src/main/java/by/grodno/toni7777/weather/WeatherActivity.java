package by.grodno.toni7777.weather;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import by.grodno.toni7777.weather.app.WeatherApp;
import by.grodno.toni7777.weather.model.WeatherData;
import by.grodno.toni7777.weather.mvp.WeatherPresenter;
import by.grodno.toni7777.weather.mvp.WeatherPresenterImp;
import by.grodno.toni7777.weather.mvp.WeatherView;
import by.grodno.toni7777.weather.network.NetworkService;
import by.grodno.toni7777.weather.network.model.WeatherDataDTO;

public class WeatherActivity extends AppCompatActivity implements View.OnClickListener, WeatherView {

    private static final String EXTRA_RX = "EXTRA_RX";
    private EditText cityName;
    private Button rxCall;
    private TextView rxResponse;
    private ProgressBar progressBar;
    private NetworkService service;
    private boolean rxCallInWorks = false;
    private WeatherPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_weather);
        cityName = (EditText) findViewById(R.id.city_name);
        rxCall = (Button) findViewById(R.id.rxCall);
        rxResponse = (TextView) findViewById(R.id.rxResponse);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        rxCall.setOnClickListener(this);
        service = ((WeatherApp) getApplication()).getNetworkService();
        presenter = new WeatherPresenterImp(this, service);
        if (savedInstanceState != null) {
            rxCallInWorks = savedInstanceState.getBoolean(EXTRA_RX);
        }

        if (BuildConfig.DEBUG) {
            cityName.setText("London");
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.rxCall:
                rxCallInWorks = true;
                presenter.loadRxData(cityName.getText().toString());
                break;
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        presenter.rxUnSubscribe();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBoolean(EXTRA_RX, rxCallInWorks);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (rxCallInWorks) {
            presenter.loadRxData("London");
        }
    }

    @Override
    public void showRxData(WeatherDataDTO data) {
        rxResponse.setText(data.toString());
        rxResponse.setVisibility(View.VISIBLE);
        rxCall.setEnabled(true);
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void showRxError(Throwable throwable) {
        Log.d("TAG", throwable.toString());
        rxResponse.setText("ERROR");
        rxResponse.setVisibility(View.VISIBLE);
        rxCall.setEnabled(true);
        progressBar.setVisibility(View.GONE);
    }

    public void showRxInProcess() {
        rxResponse.setVisibility(View.INVISIBLE);
        progressBar.setVisibility(View.VISIBLE);
        rxCall.setEnabled(false);
    }
}


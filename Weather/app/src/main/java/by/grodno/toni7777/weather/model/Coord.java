package by.grodno.toni7777.weather.model;

/**
 * Created by mymacbookpro on 6/6/16.
 */
public class Coord {

    private float lon;
    private float lat;

    public Coord() {
    }

    public Coord(float lon, float lat) {
        this.lon = lon;
        this.lat = lat;
    }

    public float getLon() {
        return lon;
    }

    public void setLon(float lon) {
        this.lon = lon;
    }

    public float getLat() {
        return lat;
    }

    public void setLat(float lat) {
        this.lat = lat;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Coord coord = (Coord) o;

        if (Float.compare(coord.lon, lon) != 0) return false;
        return Float.compare(coord.lat, lat) == 0;

    }

    @Override
    public int hashCode() {
        int result = (lon != +0.0f ? Float.floatToIntBits(lon) : 0);
        result = 31 * result + (lat != +0.0f ? Float.floatToIntBits(lat) : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Coord{" +
                "lon=" + lon +
                ", lat=" + lat +
                '}';
    }
}
